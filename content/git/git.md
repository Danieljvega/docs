---
title: Git
weight: -15
---


## Ignorar archivos

Comando para ignorar el directorio `node_modules` y para no subirlo al repositorio git:
```bash
touch .gitignore && echo "node_modules/" >> .gitignore && git rm -r --cached node_modules ; git status
```
