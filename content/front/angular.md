---
title: Angular
weight: -15
---

{{< toc >}}

## Instalación

Instalación de `angular-cli` de forma global(-g) a través de npm:
``` 
npm install -g @angular/cli
```
Creación de un nuevo proyecto:
```
ng new webapp-robin
```
Se añade un archivo de enrutado y se usa css como hoja de estilo.

Instalación de Boostrap con:
```
ng add @ng-bootstrap/ng-bootstrap --project webapp-robin
```

## Autenticación JSON Web Token (JWT)

Mediante la base del tutorial [Angular 10 JWT Authentication example with Web Api](https://bezkoder.com/angular-10-jwt-auth/) se realiza su implementación.

Para saber más sobre JWT en profundidad mirar [In-depth Introduction to JWT-JSON Web Token](https://bezkoder.com/jwt-json-web-token/)

Se aplica el [**intercambio de recursos de origen cruzado o CORS**](https://es.wikipedia.org/wiki/Intercambio_de_recursos_de_origen_cruzado) (Cross-origin resource sharing) como mecanismo que permite que se puedan solicitar **recursos restringidos**. CORS define una forma en la cual el navegador y el servidor pueden interactuar para determinar si es seguro permitir una petición de origen cruzado. Esto permite tener más libertad y funcionalidad que las peticiones de mismo origen, pero es adicionalmente más seguro que simplemente permitir todas las peticiones de origen cruzado. 

## CRUD

Tutorial seguido: [Angular 10 CRUD Application example with Web API](https://bezkoder.com/angular-10-crud-app/)

## Subida de imágenes

Para la subida de imágenes se utiliza el paquete NPM `ng2-file-upload`. Se lleva a cabo mediante la [documentación oficial de ng2-dile-upload](https://valor-software.com/ng2-file-upload/) y un tutorial de [positronX.io](https://www.positronx.io/angular-8-node-express-js-file-upload-tutorial/). También se usa `ngx-toastr` para mostrar mensajes de alerta. El módulo `ngx-toastr` depende de `@angular/animations`, por lo que se requiere su instalación. La instalación de estos paquetes se realiza mediante la siguiente instrucción:

```bash
npm install ng2-file-upload ngx-toastr @angular/animations --save
```

Se crea un nuevo componente llamado `upload-images` con el siguiente comando:

```bash
ng g c components/upload-images
```

En el archivo `upload-images.component.ts` se incluye lo siguiente:

```typescript
import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastrService } from 'ngx-toastr';

const URL = 'http://localhost:8080/api/upload';

```


Al final se ha seguido este tutorial:
https://bezkoder.com/angular-10-upload-multiple-images/