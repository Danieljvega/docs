---
title: Glosario
draft: false
weight: -10
---


## Acrónimos

* **CORS**: Cross-origin resource sharing
* **JWT**: JSON Web Token 


## Definiciones

* **CORS o Cross-origin resource sharing**: es un mecanismo que permite que se puedan solicitar **recursos restringidos**. CORS define una forma en la cual el navegador y el servidor pueden interactuar para determinar si es seguro permitir una petición de origen cruzado. Esto permite tener más libertad y funcionalidad que las peticiones de mismo origen, pero es adicionalmente más seguro que simplemente permitir todas las peticiones de origen cruzado.
* **JWT**: JSON Web Token 