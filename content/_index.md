---
title: Documentación
date: 2020-10-30
draft: false
weight: -10
---

{{< hint info >}}
**¡Página web en construcción!** 
{{< /hint >}}

## ¿Qué es RobinForGood?

Es una aplicación web que ofrece la posibilidad de **conectar a personas o colectivos que quieren donar, regalar o compartir recursos con otras personas que las necesitan o quieren reutilizar** dichos recursos. 

Aquí se recoge toda la documentación del proyecto [RobinForGood](/docs/robinforgood/introduction). Si tienes alguna duda o quieres colaborar en el proyecto, no dudes en escribir a la dirección de correo `robinforgood@protonmail.com`. ¡Gracias por tu tiempo!

## Índice de contenidos

{{< toc-tree >}}

Esta página de documentación se ha contruido con tema de Hugo Geekdoc: [documentación](https://geekdocs.de/) y [github](https://github.com/thegeeklab/hugo-geekdoc).
