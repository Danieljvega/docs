---
title: Objetivos
date: 2020-11-16
draft: false
weight: -10
---

Los objetivos se dividen en varios grupos en función del tiempo. Se han fijado tres grupos de objetivos:

+ Corto plazo: terminar antes del 1 de enero.
+ Medio plazo: terminar antes del 1 de junio.
+ Largo plazo: terminar antes del 1 de diciembre.

## Corto plazo

+ Diseño del modelo y la arquitectura del sistema completo.
+ Diseño de las medidas de seguridad y buenas prácticas.
+ Implementación de la versión funcional del código frontend y backend.
+ Despliegue mediante Kubernetes en un clúster de Raspberrys Pis.
+ Aplicación e implementación de las medidas de seguridad.
