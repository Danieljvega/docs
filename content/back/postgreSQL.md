---
title: PostgreSQL
draft: false
weight: -20
---

{{< toc >}}

## Instalación

Instalación de `postgresql` y `postgis`(ofrece soporte geográfico):

* Distribuciones basadas en Arch:
```bash
sudo pacman -Sy postgresql postgis
```  
* Distrubuciones basadas en Debian:
```bash
sudo apt install -y postgresql postgis
```

## Inicialización y configuración
Ahora se cambia a la cuenta de usuario `postgrest` y se inicializa la base de datos:
```bash
sudo su postgres -l # o sudo -u postgres -i
initdb --locale $LANG -E UTF8 -D '/var/lib/postgres/data/'
exit
```
Al iniciar la base de datos se informa que se puede iniciar el servidor de bases de datos usando:
`pg_ctl -D /var/lib/postgres/data/ -l archivo_de_registro start`


Siendo las opciones de inicialización las siguientes:
*  ` --locale` donde la configuración regional es la disponible en el sistema
*   `-E` es la codificación predeterminada para nuevas bases de datos
*   `-D` es la ubicación predeterminada para almacenar el clúster de la base de datos

Se inicia y se habilita el `servicio postgresql`:
```bash
sudo systemctl enable --now postgresql.service
```

### Creación de base de datos
Se cambia a la cuenta `postgres`: 
```bash
sudo su postgres -l
```
Y se crea la base de datos:
```bash
createdb testdb
``` 

### Comandos básicos

Para entrar en la base de datos se usa desde la cuenta de usuario postgres:
```bash
psql <nombre-bbdd>
```

Para ver toda una tabla se usa:
```
select * from <nombre-tabla>;
```

Otros comandos útiles:

Pedir ayuda:
```
=# \help
```
Conectarse a una base de datos en particular:
```
=# \c <base_de_datos>
```
Enumerar todos los usuarios y sus niveles de permiso:
```
=# \du
```
Mostrar la información resumida sobre todas las tablas de la base de datos actual:
```
=# \dt
```
Describir una tabla:
```
=# \d <nombre_tabla>  
```

Salir del shell psql:
```
=# \q o CTRL + d
```
También hay muchos más metacomandos. Para ver la ejecución de todos los metacomandos:
``` 
=# \?
``` 

### Configuración adicional

#### Restricción de los derechos de acceso al superusuario de la base de datos de forma predeterminada

Los valores predeterminados pg_hba.conf permiten que cualquier usuario local se conecte como cualquier usuario de la base de datos, incluido el superusuario de la base de datos. Es probable que esto no sea deseable, por lo que para restringir el acceso global al usuario de postgres, cambiamos la siguiente línea:
```
/var/lib/postgres/data/pg_hba.conf

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     trust
```
Por: 
```
/var/lib/postgres/data/pg_hba.conf

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             postgrest                                     trust
``` 

Se puede seguir configurando los siguientes apartados en [Optional configuration en Arch Wiki](https://wiki.archlinux.org/index.php/PostgreSQL#Configure_PostgreSQL_to_be_accessible_exclusively_through_UNIX_Sockets). Además también se recomienda seguir la documentación oficial.

## Instalación de PgAdmin

Instalación para distribuciones basadas en Arch de una interfaz gráfica de administración para PostgreSQL:
```bash
sudo pacman -Sy pgadmin4
```
Se abre directamente desde los programas instalados y se solicitará una contraseña para proteger las contraseñas y otras credenciales.

## Instalación de PgModeler

Se instala desde el repositorio de software de Manjaro.

## Enlaces consultados
* [PostgreSQL Documentation](https://www.postgresql.org/docs/13/admin.html)
* [ArchWiki – PostgreSQL](https://wiki.archlinux.org/index.php/PostgreSQL)
* [How to install PostgreSQL in Manjaro Linux ](https://lobotuerto.com/blog/how-to-install-postgresql-in-manjaro-linux/)