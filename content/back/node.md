---
title: Node
draft: false
weight: -20
---

{{< toc >}}

## Instalación

Instalación de node y npm:

* Distribuciones basadas en Arch:
```bash
sudo pacman -Sy nodejs npm
```
* Distrubuciones basadas en Debian:
```bash
sudo apt install -y nodejs npm
```

Creación de una carpeta para el proyecto:
```bash
mkdir robin-backend
cd robin-backend
```
Inicialización la aplicación node con:
```bash
npm init 
```
Instalación de los módulos:
* **express**: es el framework de nodejs para la implementación de API RESTs.
* **sequelize**: es un ORM de Node.js basado en promesas para Postgres, MySQL, MariaDB, SQLite y Microsoft SQL Server.
* **pg**: es la base de datos PostgreSQL.
* **pg-hstore**: sirve para convertir los datos en el formato hstore de PostgreSQL.
* **body-parser**: es el middleware de análisis del cuerpo de Node.js. Es responsable de analizar los cuerpos de las solicitudes entrantes en un middleware antes de manejarlo.
* **cors**: CORS (Cross-Origin Resource Sharing) es un mecanismo que permite que se puedan solicitar recursos restringidos (como por ejemplo, las tipografías) en una página web desde un dominio diferente del dominio que sirvió el primer recurso. <cite> [^1]</cite>
[^1]: [Wikipedia, Intercambio de recursos de origen cruzado](https://es.wikipedia.org/wiki/Intercambio_de_recursos_de_origen_cruzado)
```bash
npm install express sequelize pg pg-hstore body-parser cors --save
```

## Configuración de PostgreSQL y Sequelize

Se crea el directorio `config` con el archivo `db.config.js` el cual tiene en su interior:
```
module.exports = {
  HOST: "localhost",
  USER: "postgres",
  PASSWORD: "123",
  DB: "testdb",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
``` 

Los primeros cinco parámetros son para la conexión PostgreSQL. `pool` es opcional, se utilizará para la configuración de conexiones Sequelize:
* `max`: número máximo de conexiones en pool  
* `min`: número mínimo de conexiones en pool
* `idle`: tiempo máximo, en milisegundos, que una conexión puede estar inactiva antes de ser liberada
* `acquire`: tiempo máximo, en milisegundos, que el grupo intentará conectarse antes de lanzar el error

Más información en la [referencia de la API del constructor de Sequelize](https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor).

## JSON Web Token (JWT) Authentication

Teoría y flujos en: [Node.js Express + Angular 8: JWT Authentication & Authorization example](https://bezkoder.com/node-js-express-angular-jwt-auth/)

Teoría en profuncidad [JWT JSON Web Token](https://bezkoder.com/jwt-json-web-token/)

Implementación JWT en Node.js Backend en: [Node.js Express: JWT example | Token Based Authentication & Authorization](https://bezkoder.com/node-js-jwt-authentication-mysql/)

Implementación Frontend en: 
* [Angular 10 JWT Authentication example with Web Api](https://bezkoder.com/angular-10-jwt-auth/)
* [Angular 8 JWT Auth – Token based Authentication with Web Api example](https://bezkoder.com/angular-jwt-authentication/)

## Implementación JWT en Node.js

Primero se instalan los módulos `jsonwebtoken` y `bcryptjs`:

```bash
npm install jsonwebtoken bcryptjs --save 
``` 
Se configura el `server.js` para activar CORS con `origin` en `http://localhost:8081`.


## CRUD

Se toma como referencia el tutorial de bezkoder [Node.js Express & PostgreSQL: CRUD Rest APIs example with Sequelize](https://bezkoder.com/node-express-sequelize-postgresql/).

## Asociaciones

Se sigue la [documentación oficial de sequelize](https://sequelize.org/master/manual/assocs.html) y el ejemplo descrito en el tutorial CRUD de bezkoder.

También pueden servir de referencia los siguientes enlaces:
+ [Node.js Sequelize Associations: One-to-Many example](https://bezkoder.com/sequelize-associate-one-to-many/)
+ [Node.js Sequelize Associations: Many-to-Many example](https://bezkoder.com/sequelize-associate-many-to-many/)


## Subida de imágenes

Se lleva a cabo mediante la [documentación oficial de ng2-dile-upload](https://valor-software.com/ng2-file-upload/) y un tutorial de [positronX.io](https://www.positronx.io/angular-8-node-express-js-file-upload-tutorial/). Se utiliza [multer](https://www.npmjs.com/package/multer) que es un **middleware** de node.js para el **manejo datos de formularios con multiparte**, el cual se se usa principalmente para **cargar archivos**. Se instala mediante el siguiente comando:

```bash
npm install multer --save
```

Se crea una nuevo archivo de rutas `routes/upload.routes.js` con el siguiente contenido:

```typescript
const multer = require('multer')

module.exports = app => {

  var router = require("express").Router();
  const PATH_IMAGES = './uploads';

  let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, PATH);
    },
    filename: (req, file, cb) => {
      cb(null, Date.now() + '-' + file.fieldname)
    }
  });

  let upload = multer({
    storage: storage
  });

  app.get('/api', function (req, res) {
    res.end('File catcher');
  });
  
  // POST File
  app.post('/api/upload', upload.single('image'), function (req, res) {
    if (!req.file) {
        console.log("No file is available!");
        return res.send({
            success: false
        });
  
    } else {
        console.log('File is available!');
        return res.send({
            success: true
        })
    }
  });

}
```

Para poder acceder a estas rutas es necesario importarlas en `server.js` con la siguiente línea:

```typescript
require('./routes/upload.routes')(app);
```

 

Al final se ha seguido el tutorial:
https://bezkoder.com/node-js-express-file-upload/
Mirar este enlace para guardarlo en la bbdd
https://bezkoder.com/node-js-upload-image-mysql/
Subida múltiple de archivos:
https://bezkoder.com/node-js-upload-multiple-files/
Para redimensionarlas con sharp:
https://bezkoder.com/node-js-upload-resize-multiple-images/